package mc322.lab01.calculator;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        Calculator.printMenu();
        Calculator calc = new Calculator();

        int numberOfOperations = Calculator.Operation.values().length;
        int choice = getOperationID(input);

        while (0 < choice && choice <= numberOfOperations + 1) {
            if (choice == numberOfOperations + 1) {
                Calculator.printMenu();
            } else {
                Calculator.Operation operation = Calculator.Operation.values()[choice - 1];
                Calculator.Operator operator;

                switch (operation) {
                    case ADDITION:
                        operator = calc.new Addition();
                        break;
                    case SUBTRACTION:
                        operator = calc.new Subtraction();
                        break;
                    case MULTIPLICATION:
                        operator = calc.new Multiplication();
                        break;
                    case DIVISION:
                        operator = calc.new Division();
                        break;
                    case FACTORIAL:
                        operator = calc.new Factorial();
                        break;
                    case CHECKPRIME:
                        operator = calc.new CheckPrime();
                        break;
                    default:
                        return;
                }

                if (operator.operation.type == Calculator.OperationType.BINARY) {
                    int x = getValue(input);
                    int y = getValue(input);

                    printResult(operator.operate(x, y));
                } else {
                    int x = getValue(input);

                    printResult(operator.operate(x));
                }
            }

            choice = getOperationID(input);
        }

        input.close();
    }

    public static int getOperationID(Scanner input) {
        System.out.print("Select one operation: ");
        int choice = input.nextInt();
        return choice;
    }

    public static int getValue(Scanner input) {
        System.out.print("Value: ");
        int value = input.nextInt();
        return value;
    }

    public static void printResult(int result) {
        System.out.println("Result: " + result);
    }
}
