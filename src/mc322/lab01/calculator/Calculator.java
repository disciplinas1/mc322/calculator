package mc322.lab01.calculator;

public class Calculator {

    public static void printMenu() {
        int numberOfOperations = Operation.values().length;

        printMenuLine(0, "Exit");
        for (int i = 1; i <= Operation.values().length; i++) {
            Operation op = Operation.values()[i - 1];
            printMenuLine(i, op.label + " - Type: " + op.type);
        }

        printMenuLine(numberOfOperations + 1, "Print this Menu");
    }

    public static void printMenuLine(int i, String label) {
        System.out.println("ID: " + i + " - " + label);
    }

    public enum OperationType {
        UNARY,
        BINARY;
    }

    public enum Operation {
        ADDITION("Addition", OperationType.BINARY),
        SUBTRACTION("Subtraction", OperationType.BINARY),
        MULTIPLICATION("Multiplication", OperationType.BINARY),
        DIVISION("Division", OperationType.BINARY),
        FACTORIAL("Factorial", OperationType.UNARY),
        CHECKPRIME("Check Prime (0: false, 1: true)", OperationType.UNARY);

        String label;
        OperationType type;

        private Operation(String label, OperationType type) {
            this.label = label;
            this.type = type;
        }
    }

    public abstract class Operator {
        public Operation operation;

        public Operator(Operation operation) {
            this.operation = operation;
        }

        public void setOperation(Operation operation) {
            this.operation = operation;
        }

        public Operation getOperation() {
            return this.operation;
        }

        public abstract int operate(int... args);
    }

    public abstract class UnaryOperator extends Operator {

        public UnaryOperator(Operation operation) {
            super(operation);
        }

        public int operate(int... args) {
            int x = args[0];

            return operateUnary(x);
        }

        public abstract int operateUnary(int x);
    }

    abstract class BinaryOperator extends Operator {

        public BinaryOperator(Operation operation) {
            super(operation);
        }

        public int operate(int... args) {
            int x = args[0];
            int y = args[1];

            return operateBinary(x, y);
        }

        public abstract int operateBinary(int x, int y);
    }

    public class Addition extends BinaryOperator {

        public Addition() {
            super(Operation.ADDITION);
        }

        @Override
        public int operateBinary(int x, int y) {
            return x + y;
        }
    }

    public class Subtraction extends BinaryOperator {

        public Subtraction() {
            super(Operation.SUBTRACTION);
        }

        @Override
        public int operateBinary(int x, int y) {
            return x - y;
        }
    }

    public class Multiplication extends BinaryOperator {

        public Multiplication() {
            super(Operation.MULTIPLICATION);
        }

        @Override
        public int operateBinary(int x, int y) {
            return x * y;
        }
    }

    public class Division extends BinaryOperator {

        public Division() {
            super(Operation.DIVISION);
        }

        @Override
        public int operateBinary(int x, int y) {
            return x / y;
        }
    }

    public class Factorial extends UnaryOperator {

        public Factorial() {
            super(Operation.FACTORIAL);
        }

        @Override
        public int operateUnary(int x) {
            if (x < 0) {
                System.out.println("Error: trying to get factorial of " + x);
                return 0;
            }

            int ret;
            if (x == 0) {
                ret = 1;
            } else {
                ret = x * this.operateUnary(x - 1);
            }

            return ret;
        }
    }

    public class CheckPrime extends UnaryOperator {

        public CheckPrime() {
            super(Operation.CHECKPRIME);
        }

        @Override
        public int operateUnary(int x) {
            if (x < 0) {
                System.out.println("Error: trying to check if " + x + " is prime");
                return 0;
            }

            if (x <= 1) {
                return 0;
            } else if (x <= 3) {
                return 1;
            } else if (x % 2 == 0 || x % 3 == 0) {
                return 0;
            } else {
                int div = 5;
                while (Math.pow(div, 2) <= x) {
                    if (x % div == 0 || x % (div + 2) == 0) {
                        return 0;
                    }

                    div += 6;
                }

                return 1;
            }
        }
    }
}
